.PHONY: all clean fclean re

CC=clang
CFLAGS=-Wall -Wextra -Werror -Ilibft/includes -Iinclude

SHAREDSRC=errors.c share.c
SHAREDOBJ=$(SHAREDSRC:.c=.o)

CLIENTSRC=client_main.c
CLIENTOBJ=$(CLIENTSRC:.c=.o)

SERVERSRC=server_main.c server_funcs.c
SERVEROBJ=$(SERVERSRC:.c=.o)

SRC=$(CLIENTSRC) $(SERVERSRC) $(SHAREDSRC)
OBJ=$(CLIENTOBJ) $(SERVEROBJ) $(SHAREDOBJ)

SRCDIR=src
OBJDIR=obj

LIBFT=libft/libft.a

CLIENT=client
SERVER=server

all: $(OBJDIR) SUBMODULE $(SERVER) $(CLIENT)

$(OBJDIR):
	@mkdir -p $@

SUBMODULE:
	@git submodule update --init

$(SERVER): $(LIBFT) $(addprefix $(OBJDIR)/, $(SERVEROBJ)) $(addprefix $(OBJDIR)/, $(SHAREDOBJ))
	@echo "Compiling server"
	$(CC) $(CFLAGS) -o $@ $^

$(CLIENT): $(LIBFT) $(addprefix $(OBJDIR)/, $(CLIENTOBJ)) $(addprefix $(OBJDIR)/, $(SHAREDOBJ))
	@echo "Compiling client"
	$(CC) $(CFLAGS) -o $@ $^ -lpthread

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $<

debug: $(LIBFT) $(addprefix $(OBJDIR)/, $(SERVEROBJ)) $(addprefix $(OBJDIR)/, $(CLIENTOBJ)) $(addprefix $(OBJDIR)/, $(SHAREDOBJ))
	$(CC) $(CFLAGS) -o $(SERVER) -g $(addprefix $(SRCDIR)/, $(SERVERSRC)) $(addprefix $(SRCDIR)/, $(SHAREDSRC)) $(LIBFT)
	$(CC) $(CFLAGS) -o $(CLIENT) -g $(addprefix $(SRCDIR)/, $(CLIENTSRC)) $(addprefix $(SRCDIR)/, $(SHAREDSRC)) $(LIBFT)

$(LIBFT):
	@$(MAKE) -C libft

clean:
	@-rm -rf $(OBJDIR)
	@$(MAKE) -C libft clean

fclean:
	@-rm -rf $(OBJDIR)
	@-rm -f $(CLIENT) $(SERVER)
	@$(MAKE) -C libft fclean

re: fclean all
