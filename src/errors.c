/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 14:43:52 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/23 16:52:11 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shared_funcs.h"
#include "ft_printf.h"

void		errmsg(char *str)
{
	ft_printf("%s\n", str);
	exit(EXIT_FAILURE);
}

void		perrmsg(char *str)
{
	perror(str);
	ft_printf("%d\n", errno);
	exit(EXIT_FAILURE);
}
