/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   share.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 17:52:51 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 18:00:54 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"
#include "shared_funcs.h"

int		max(int *fds, int count)
{
	int		i;
	int		ret;

	i = 0;
	ret = 0;
	while (i < count)
	{
		if (ret < fds[i])
			ret = fds[i];
		i++;
	}
	return (ret);
}

fd_set	*useselect(t_list *lst, int mode, int sec)
{
	struct timeval	time_m;
	static fd_set	fds;
	int				i;
	t_user			*usr;

	time_m.tv_sec = sec;
	time_m.tv_usec = 10000;
	FD_ZERO(&fds);
	i = -1;
	while (lst)
	{
		usr = (t_user *)lst->content;
		i = (i > usr->sock) ? i : usr->sock;
		FD_SET(usr->sock, &fds);
		lst = lst->next;
	}
	if (mode == SELECTREAD)
		i = select(i + 1, &fds, NULL, NULL, &time_m);
	else if (mode == SELECTWRITE)
		i = select(i + 1, NULL, &fds, NULL, &time_m);
	else if (mode == SELECTEXCEPT)
		i = select(i + 1, NULL, NULL, &fds, &time_m);
	else
		return (NULL);
	if (i < 1)
		return (NULL);
	return (&fds);
}

fd_set	*useselect2(int sock, int mode, int sec)
{
	struct timeval	time_m;
	static fd_set	fds;
	int				i;

	time_m.tv_sec = sec;
	time_m.tv_usec = 10000;
	FD_ZERO(&fds);
	FD_SET(sock, &fds);
	if (mode == SELECTREAD)
		i = select(sock + 1, &fds, NULL, NULL, &time_m);
	else if (mode == SELECTWRITE)
		i = select(sock + 1, NULL, &fds, NULL, &time_m);
	else if (mode == SELECTEXCEPT)
		i = select(sock + 1, NULL, NULL, &fds, &time_m);
	else
		return (NULL);
	if (i < 1)
		return (NULL);
	return (&fds);
}

int		getl(int fd, char **str)
{
	static char		buff[1024];
	int				ret;

	ret = read(fd, buff, 1023);
	while (ret == -1 && errno == EAGAIN)
		ret = read(fd, buff, 1023);
	if (ret == -1)
		return (-1);
	if (ret == 0)
		return (0);
	buff[ret] = 0;
	*str = buff;
	return (1);
}
