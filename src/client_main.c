/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_main.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 14:36:47 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 18:52:41 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shared_funcs.h"
#include "libft.h"
#include "ft_printf.h"
#include <errno.h>
#include <pthread.h>

void	writename(int sock, char **env)
{
	int		i;

	i = 0;
	while (env[i])
	{
		if (ft_strstr(env[i], "HOME=/Users/"))
			break ;
		++i;
	}
	if (env[i] == NULL)
		exit(0);
	while (!useselect2(sock, SELECTWRITE, 0))
		;
	if (write(sock, env[i] + 12, ft_strlen(env[i] + 12)) == -1)
		perrmsg("write()");
}

void	*readit(void *s)
{
	char			buff[500];
	int				ret;
	int				sock;

	sock = *(int *)s;
	while (1)
	{
		while (useselect2(sock, SELECTREAD, 1))
		{
			ret = read(sock, buff, 499);
			if (ret == -1)
				perrmsg("read()");
			buff[ret] = 0;
			if (ret)
			{
				write(1, "\a", 1);
				write(1, buff, ft_strlen(buff));
				usleep(100000);
				write(1, "\a", 1);
			}
		}
	}
	return (NULL);
}

int		writeit(int sock)
{
	int		ret;
	char	*str;
	int		strsize;

	str = NULL;
	ft_printf("You: ");
	ret = get_next_line(0, &str);
	if (ret == 0)
		exit(EXIT_SUCCESS);
	if (ret == -1)
		perrmsg("getnextline()");
	strsize = ft_strlen(str);
	if (strsize > 499)
		write(2, "Too long message\n", 17);
	else
		write(sock, str, strsize);
	return (1);
}

int		the_cycle(int mainsock)
{
	pthread_t		th;
	pthread_attr_t	at;
	int				sock;

	sock = mainsock;
	pthread_attr_init(&at);
	pthread_create(&th, &at, readit, &sock);
	while (1)
	{
		if (useselect2(mainsock, SELECTWRITE, 1))
			if (writeit(mainsock) == 0)
				break ;
	}
	if (pthread_cancel(th) == -1)
		perror("pthread_cancel()");
	return (0);
}

int		main(int ac, char **av, char **env)
{
	t_sockaddr_in	saddr;
	int				mainsock;

	if (ac != 3)
		errmsg("usage: ./client \"ip \" \"port\"");
	if ((mainsock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		perrmsg("socket()");
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(ft_atoi(av[2]));
	if (inet_aton(av[1], &saddr.sin_addr) == -1)
		perrmsg("inet_aton()");
	if (connect(mainsock, (t_sockaddr *)&saddr, sizeof(saddr)) == -1)
		perrmsg("connect()");
	if (fcntl(mainsock, F_SETFL, O_NONBLOCK) == -1)
		perrmsg("fcntl()");
	writename(mainsock, env);
	the_cycle(mainsock);
	close(mainsock);
	return (0);
}
