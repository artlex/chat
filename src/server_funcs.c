/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_funcs.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 20:46:42 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 17:50:07 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void	delusr(void *cont, size_t a)
{
	t_user	*user;

	if (a)
		;
	user = (t_user *)cont;
	close(user->sock);
	free(user->nickname);
	free(cont);
}

void	deluser(t_list **alst, t_list *lst)
{
	t_list	*tmp;

	tmp = *alst;
	if (lst == tmp)
	{
		*alst = tmp->next;
		ft_lstdelone(&lst, delusr);
		return ;
	}
	while (tmp->next && tmp->next != lst)
		tmp = tmp->next;
	tmp->next = lst->next;
	ft_lstdelone(&lst, delusr);
}

void	saveuser(t_user *usr, t_list **al)
{
	t_user		user;
	t_list		*lst;
	int			ret;
	char		buff[500];

	user.sock = usr->sock;
	user.nickname = NULL;
	user.sockaddr = usr->sockaddr;
	user.socklen = usr->socklen;
	while (!useselect2(usr->sock, SELECTREAD, 0))
		;
	ret = read(usr->sock, buff, 499);
	while (ret == -1 && errno == EAGAIN)
		ret = read(usr->sock, buff, 499);
	if (ret == -1)
		perrmsg("read()");
	buff[ret] = 0;
	user.nickname = ft_strdup(buff);
	lst = ft_lstnew(&user, sizeof(user));
	ft_lstadd(al, lst);
}
