/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_main.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 14:36:54 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/28 16:26:57 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static const int	g_max_users = 15;
static t_list		*g_usr_list = NULL;
static int			g_num_socks = 0;

void	sendit(char *msg, t_list *ignore)
{
	int				ret;
	t_list			*lst;
	t_user			*user;
	fd_set			*fds;

	lst = g_usr_list;
	if (!(fds = useselect(lst, SELECTWRITE, 0)))
		return ;
	while (lst)
	{
		user = (t_user *)lst->content;
		if (lst != ignore && FD_ISSET(user->sock, fds))
		{
			ret = write(user->sock, msg, ft_strlen(msg));
			if (ret == -1)
				perrmsg("write()");
		}
		lst = lst->next;
	}
}

void	readit()
{
	int				ret;
	char			*str;
	t_list			*lst;
	t_user			*user;
	fd_set			*fds;

	str = NULL;
	lst = g_usr_list;
	if (!(fds = useselect(lst, SELECTREAD, 0)))
		return ;
	while (lst)
	{
		user = lst->content;
		if (FD_ISSET(user->sock, fds))
		{
			if ((ret = getl(user->sock, &str)) < 1)
			{
				g_num_socks--;
				if (ret == -1)
					perror("read()");
				deluser(&g_usr_list, lst);
			}
			else
			{
				ft_printf("%s: %s\n", user->nickname, str);
				ft_asprintf(&str, "\b\b\b\b\b%s: %s\nYou: ", user->nickname, str);
				sendit(str, lst);
			}
			break ;
		}
		lst = lst->next;
	}
}

int		accept_queue(int mainsock)
{
	t_user			save;
	fd_set			*fds;
	t_list			*lst;

	while (1)
	{
		lst = g_usr_list;
		if (useselect2(mainsock, SELECTREAD, 0))
		{
			if ((save.sock = accept(mainsock,
				(t_sockaddr *)(&save.sockaddr), &save.socklen)) == -1)
				errmsg("sock()");
			else
			{
				saveuser(&save, &g_usr_list);
				g_num_socks++;
			}
		}
		if (g_num_socks > 0 && (fds = useselect(lst, SELECTREAD, 0)))
			readit();
	}
	return (0);
}

int		main(int ac, char **av)
{
	t_sockaddr_in	saddr;
	int				mainsock;

	if (ac != 2)
		errmsg("usage: ./server \"port\"");
	if ((mainsock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		perrmsg("socket()");
	if (fcntl(mainsock, F_SETFL, O_NONBLOCK) == -1)
		perrmsg("fcntl()");
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(ft_atoi(av[1]));
	if (inet_aton("127.0.0.1", &saddr.sin_addr) == -1)
		perrmsg("inet_aton()");
	saddr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(mainsock, (t_sockaddr *)&saddr, sizeof(saddr)) == -1)
		perrmsg("bind()");
	if (listen(mainsock, g_max_users) == -1)
		perrmsg("listen()");
	accept_queue(mainsock);
	close(mainsock);
	return (0);
}
