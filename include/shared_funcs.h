/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shared_funcs.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 14:42:02 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/27 18:01:07 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHARED_FUNCS_H
# define SHARED_FUNCS_H

# include <sys/socket.h>
# include <sys/types.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <stdio.h>
# include <fcntl.h>
# include <errno.h>
# include <time.h>
# include "libft.h"

# define SELECTREAD 1
# define SELECTWRITE 2
# define SELECTEXCEPT 3

typedef struct sockaddr_in	t_sockaddr_in;
typedef struct sockaddr		t_sockaddr;

void						perrmsg(char *str);
void						errmsg(char *str);

int							max(int *fds, int count);
fd_set						*useselect(t_list *lst, int mode, int sec);
fd_set						*useselect2(int sock, int mode, int sec);

int							getl(int fd, char **str);

#endif
