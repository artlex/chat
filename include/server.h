/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 20:15:25 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/26 18:15:43 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_H
# define SERVER_H

# include "shared_funcs.h"
# include "libft.h"

typedef struct	s_user
{
	t_sockaddr_in	sockaddr;
	char			*nickname;
	socklen_t		socklen;
	int				sock;
}				t_user;

void			saveuser(t_user *usr, t_list **al);

void			delusr(void *cont, size_t);
void			deluser(t_list **alst, t_list *lst);

#endif
